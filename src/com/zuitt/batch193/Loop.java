package com.zuitt.batch193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Loop {

    public static void main(String[] args) {

        int counter = 1;
        Scanner in = new Scanner(System.in);

        int num = 1;
        long x = 1;

        try {
            System.out.println("Input an integer whose factorial will be computed: ");
            num = in.nextInt();
            x = num;

            if (num > 0) {
                System.out.println("The number you entered is: " + num);
                while (num > 0){
                    if(num-1 != 0){
//                        System.out.println("");
//                        System.out.println("loop: "+counter);
//                        System.out.println("num: "+num);
//                        System.out.println("x: "+x);
                        x = x * (num-1);
//                        System.out.println("num-1: "+(num-1));
//                        System.out.println("x*num-1: "+x);
                        num--;
                        counter++;
//                        System.out.println("loop "+counter+" end");
//                        System.out.println("");
                    } else {
                        num--;
                        System.out.println("The factorial of " + counter + " is: " + x);
                    }
                }
            } else if (num < 0) {
                System.out.println("The number you entered is: " + num);
                System.out.println("Please enter a positive integer");
            } else if (num == 0) {
                System.out.println("The number you entered is: " + num);
                System.out.println("The factorial of 0 is 1!");
            }
        } catch (InputMismatchException e) {
            System.out.println("Must enter a number!");
        }
        catch (Exception e) {
            System.out.println("Invalid Input.");
        } finally {
            counter = 0;
        }

    }

}
